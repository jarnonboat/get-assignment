package com.getinterview;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinterview.utils.WriteToText;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.transforms.windowing.*;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TypeDescriptors;
import org.joda.time.Duration;
import org.joda.time.Instant;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is associated to pipeline (1) in the document.
 * This pipeline based on ETL concept. It will do transformation
 * and aggregation before loading to sink output.
 */
public class BookingCountTimePipeline {

    public interface BookingCountOptions extends PipelineOptions {

        /**
         * Set this required option to specify where to read the output.
         */
        @Description("Path of the file to read from")
        @Default.String("input/sampleNormalCase1.json")
        @Validation.Required
        String getInput();

        void setInput(String value);

        /**
         * Set this required option to specify where to write the output.
         */
        @Description("Path of the file to write to")
        @Default.String("output/output.csv")
        @Validation.Required
        String getOutput();

        void setOutput(String value);

        @Description("Numeric value of fixed window duration for analysis, in minutes")
        @Default.Integer(60)
        @Validation.Required
        Integer getWindowDuration();

        void setWindowDuration(Integer value);

        @Description("Numeric value of allowed data lateness, in minutes")
        @Default.Integer(120)
        @Validation.Required
        Integer getAllowedLateness();

        void setAllowedLateness(Integer value);

        @Description("Boolean value to write key in output file (useful for debugging)")
        @Default.Boolean(false)
        @Validation.Required
        Boolean getWriteKey();

        void setWriteKey(Boolean value);
    }

    /**
     * Configuration for output file including
     * window time, key, total booking count
     */
    private static Map<String, WriteToText.FieldFn<KV<String, Integer>>> configureOutput() {
        Map<String, WriteToText.FieldFn<KV<String, Integer>>> config = new HashMap<>();
        config.put(
                "00_window_start",
                (c, w) -> {
                    IntervalWindow window = (IntervalWindow) w;
                    return window.end().toString();
                });
        config.put("01_key", (c, w) -> c.element().getKey());
        config.put("02_total_booking", (c, w) -> c.element().getValue());
        return config;
    }


    /**
     * Parses the raw booking event json into BookingMessage objects.
     * Perform type checking and completeness checking on input message
     */
    protected static class ParseBookingEventFn extends DoFn<String, BookingMessage> {

        // Log and count parse errors. // uncomment below line to see parsing log in console
        // private static final Logger LOG = LoggerFactory.getLogger(ParseBookingEventFn.class);
        private final Counter numParseErrors = Metrics.counter("main", "ParseErrors");

        @ProcessElement
        public void processElement(ProcessContext c) {
            try {
                BookingMessage b = new ObjectMapper().readValue(c.element(), BookingMessage.class);
                b.checkIfComplete();
                c.output(b);
            } catch (Exception e) {
                numParseErrors.inc();
                // LOG.info("Parse error on " + c.element() + ", " + e.getMessage());
            }
        }
    }

    /**
     * Extract the key from booking message and map to 1.
     * Count the total of booking message per key
     */
    protected static class ExtractAndCountBooking extends PTransform<PCollection<BookingMessage>, PCollection<KV<String, Integer>>> {

        private final String field;

        ExtractAndCountBooking(String field) {
            this.field = field;
        }

        @Override
        public PCollection<KV<String, Integer>> expand(PCollection<BookingMessage> gameInfo) {

            return gameInfo
                    .apply(
                            MapElements.into(
                                    TypeDescriptors.kvs(TypeDescriptors.strings(), TypeDescriptors.integers()))
                                    .via((BookingMessage bInfo) -> KV.of(bInfo.getKey(field), 1)))
                    .apply(Sum.integersPerKey());
        }
    }

    /**
     * Wrapper class to apply ExtractAndCountBooking function
     * over streaming input data using windowing, watermark and trigger
     * We will also get early (speculative) results as well as cumulative
     * processing of late data.
     */
    public static class CountBookingType
            extends PTransform<PCollection<BookingMessage>, PCollection<KV<String, Integer>>> {
        private final Duration teamWindowDuration;
        private final Duration allowedLateness;

        CountBookingType(Duration teamWindowDuration, Duration allowedLateness) {
            this.teamWindowDuration = teamWindowDuration;
            this.allowedLateness = allowedLateness;
        }

        @Override
        public PCollection<KV<String, Integer>> expand(PCollection<BookingMessage> infos) {
            return infos
                    .apply(
                            "TotalBookingCountFixedWindows",
                            Window.<BookingMessage>into(FixedWindows.of(teamWindowDuration))
                                    .triggering(
                                            AfterWatermark.pastEndOfWindow()
                                                    .withEarlyFirings(
                                                            AfterProcessingTime.pastFirstElementInPane()
                                                                    .plusDelayOf(Duration.standardMinutes(5)))
                                                    .withLateFirings(
                                                            AfterProcessingTime.pastFirstElementInPane()
                                                                    .plusDelayOf(Duration.standardMinutes(10))))
                                    .withAllowedLateness(allowedLateness)
                                    .accumulatingFiredPanes())
                    .apply("ExtractBookingCount", new ExtractAndCountBooking("get-report"));
        }
    }

    public static void main(String[] args) {
        BookingCountOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(BookingCountOptions.class);
        Pipeline pipeline = Pipeline.create(options);


        PCollection<BookingMessage> BookingEvent = pipeline
                .apply(TextIO.read().from(options.getInput()))
                .apply(ParDo.of(new ParseBookingEventFn()));

        BookingEvent
                .apply("AddEventTimestamps",
                        WithTimestamps.of((BookingMessage b) -> new Instant(b.getInstant_event_timestamp())))
                .apply("CountBookingType",
                        new CountBookingType(
                                Duration.standardMinutes(options.getWindowDuration()),
                                Duration.standardMinutes(options.getAllowedLateness())))
                .apply("WriteTotalBookingCount",
                        new WriteToText<>(options.getOutput(), configureOutput(), true, options.getWriteKey()));


        pipeline.run().waitUntilFinish();
    }
}
