package com.getinterview;

import java.io.Serializable;
import java.time.Instant;

public class BookingMessage implements Serializable {
    private String order_number;
    private String service_type;
    private String driver_id;
    private String customer_id;
    private String service_area_name;
    private String payment_type;
    private String status;
    private String event_timestamp;

    public BookingMessage() {
    }

    public BookingMessage(String order_number,
                          String service_type,
                          String driver_id,
                          String customer_id,
                          String service_area_name,
                          String payment_type,
                          String status,
                          String event_timestamp) {
        this.order_number = order_number;
        this.service_type = service_type;
        this.driver_id = driver_id;
        this.customer_id = customer_id;
        this.service_area_name = service_area_name;
        this.payment_type = payment_type;
        this.status = status;
        this.event_timestamp = event_timestamp;
    }

    public String getOrder_number() {
        return order_number;
    }

    public String getService_type() {
        return service_type;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public String getService_area_name() {
        return service_area_name;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public String getStatus() {
        return status;
    }

    public String getEvent_timestamp() {
        return event_timestamp;
    }

    public Long getInstant_event_timestamp() {
        return Instant.parse(event_timestamp).toEpochMilli();
    }

    public String getKey(String keyname) {
        if ("get-report".equals(keyname)) {
            return this.service_type + "," +
                    this.service_area_name + "," +
                    this.payment_type + "," +
                    this.status;
        } else { // return service_type as default
            return this.service_type;
        }
    }

    public boolean checkIfComplete() {
        if (order_number == null || service_type == null || driver_id == null || customer_id == null || service_area_name == null || payment_type == null || status == null || event_timestamp == null) {
            throw new IllegalArgumentException("The mandatory field is not defined !");
        }
        if (order_number.isEmpty() || service_type.isEmpty() || driver_id.isEmpty() || customer_id.isEmpty() || service_area_name.isEmpty() || payment_type.isEmpty() || status.isEmpty() || event_timestamp.isEmpty()) {
            throw new IllegalArgumentException("The mandatory field is not defined !");
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof BookingMessage)) {
            return false;
        }

        BookingMessage bookingMessage = (BookingMessage) o;

        return (order_number != null ? order_number.equals(bookingMessage.order_number) : bookingMessage.order_number == null)
                && (service_type != null ? service_type.equals(bookingMessage.service_type) : bookingMessage.service_type == null)
                && (driver_id != null ? driver_id.equals(bookingMessage.driver_id) : bookingMessage.driver_id == null)
                && (customer_id != null ? customer_id.equals(bookingMessage.customer_id) : bookingMessage.customer_id == null)
                && (service_area_name != null ? service_area_name.equals(bookingMessage.service_area_name) : bookingMessage.service_area_name == null)
                && (payment_type != null ? payment_type.equals(bookingMessage.payment_type) : bookingMessage.payment_type == null)
                && (status != null ? status.equals(bookingMessage.status) : bookingMessage.status == null)
                && (event_timestamp != null ? event_timestamp.equals(bookingMessage.event_timestamp) : bookingMessage.event_timestamp == null);
    }

    @Override
    public int hashCode() {
        int result = order_number != null ? order_number.hashCode() : 0;
        result = 31 * result;
        return result;
    }

}
