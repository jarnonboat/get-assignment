package com.getinterview;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.SerializableCoder;
import org.apache.beam.sdk.extensions.jackson.ParseJsons;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.*;

/**
 * This class is associated to pipeline (2) in the document.
 * This pipeline based on ELT concept. It will stream the data
 * to output sink and do transformation in there.
 */
public class BookingCountPipelineELT {
    public interface BookingCountOptions extends PipelineOptions {

        /**
         * Set this required option to specify where to read the output.
         */
        @Description("Path of the file to read from")
        @Validation.Required
        String getInput();

        void setInput(String value);

        /**
         * Set this required option to specify where to write the output.
         */
        @Description("Path of the file to write to")
        @Validation.Required
        String getOutput();

        void setOutput(String value);
    }

    public static class FormatAsTextFn extends SimpleFunction<BookingMessage, String> {
        @Override
        public String apply(BookingMessage input) {
            return input.getOrder_number() + "," +
                    input.getCustomer_id() + "," +
                    input.getDriver_id() + "," +
                    input.getService_area_name() + "," +
                    input.getPayment_type() + "," +
                    input.getService_type() + "," +
                    input.getStatus() + "," +
                    input.getEvent_timestamp();
        }
    }

    public static void main(String[] args) {
        BookingCountOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(BookingCountOptions.class);
        Pipeline p = Pipeline.create(options);

        p.apply("read my file", TextIO.read().from("input/*.json"))
                .apply(ParseJsons.of(BookingMessage.class))
                .setCoder(SerializableCoder.of(BookingMessage.class))
                .apply(MapElements.via(new FormatAsTextFn()))
                .apply("write to csv", TextIO.write().to("output/output.csv"));

        p.run().waitUntilFinish();
    }
}
