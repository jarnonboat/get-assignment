package com.getinterview.utils;

import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PDone;

/**
 * Mock up for BigQuery writer. Using this kind of plug and play design, we can create another
 * writer and use it as output writer
 */

public class WriteToBigQuery<InputT> extends PTransform<PCollection<InputT>, PDone> {
    @Override
    public PDone expand(PCollection<InputT> input) {
        // Just mock up
        return null;
    }
}
