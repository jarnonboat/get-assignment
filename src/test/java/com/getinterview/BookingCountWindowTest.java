package com.getinterview;

import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.testing.TestStream;
import org.apache.beam.sdk.transforms.windowing.IntervalWindow;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TimestampedValue;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class BookingCountWindowTest implements Serializable {
    private static final Duration ALLOWED_LATENESS = Duration.standardHours(1);
    private static final Duration TEAM_WINDOW_DURATION = Duration.standardMinutes(20);
    private Instant baseTime = new Instant(0);

    /**
     * Some example users, on two separate teams.
     */
    private enum TestMessage {
        ONE("AP-1", "GET_DELIVERY", "driver-123", "customer-123", "JAKARTA", "GET_CASH", "RIDING"),
        TWO("AP-1", "GET_DELIVERY", "driver-123", "customer-123", "JAKARTA", "GET_CASH", "PENDING"),
        THREE("AP-3", "GET_DELIVERY", "driver-123", "customer-123", "JAKARTA", "GET_CASH", "PENDING"),
        FOUR("AP-4", "GET_WIN", "driver-123", "customer-123", "BANGKOK", "GET_CASH", "COMPLETED");

        private final String order_number;
        private final String service_type;
        private final String driver_id;
        private final String customer_id;
        private final String service_area_name;
        private final String payment_type;
        private final String status;

        TestMessage(String order_number,
                    String service_type,
                    String driver_id,
                    String customer_id,
                    String service_area_name,
                    String payment_type,
                    String status) {
            this.order_number = order_number;
            this.service_type = service_type;
            this.driver_id = driver_id;
            this.customer_id = customer_id;
            this.service_area_name = service_area_name;
            this.payment_type = payment_type;
            this.status = status;
        }

        public String getOrder_number() {
            return order_number;
        }

        public String getService_type() {
            return service_type;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public String getService_area_name() {
            return service_area_name;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public String getStatus() {
            return status;
        }
    }

    private TimestampedValue<BookingMessage> event(TestMessage message, Duration baseTimeOffset) {
        return TimestampedValue.of(
                new BookingMessage(
                        message.getOrder_number(),
                        message.getService_type(),
                        message.getDriver_id(),
                        message.getCustomer_id(),
                        message.getService_area_name(),
                        message.getPayment_type(),
                        message.getStatus(),
                        baseTime.plus(baseTimeOffset).toString()
                ), baseTime.plus(baseTimeOffset)
        );
    }

    static final List<KV<String, Integer>> BOOKING_COUNT =
            Arrays.asList(
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,RIDING", 2),
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,PENDING", 3),
                    KV.of("GET_WIN,BANGKOK,GET_CASH,COMPLETED", 1)
            );

    static final List<KV<String, Integer>> BOOKING_COUNT_SPECULATIVE =
            Arrays.asList(
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,PENDING", 2),
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,RIDING", 1),
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,PENDING", 1),
                    KV.of("GET_WIN,BANGKOK,GET_CASH,COMPLETED", 1),
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,PENDING", 3),
                    KV.of("GET_WIN,BANGKOK,GET_CASH,COMPLETED", 1),
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,RIDING", 2)
            );

    @Rule
    public TestPipeline p = TestPipeline.create();

    @Test
    public void testBookingCountOnTime() {
        TestStream<BookingMessage> createEvents = TestStream.create(AvroCoder.of(BookingMessage.class))
                .advanceWatermarkTo(baseTime)
                .addElements(
                        event(TestMessage.ONE, Duration.standardSeconds(3)),
                        event(TestMessage.TWO, Duration.standardMinutes(1)),
                        event(TestMessage.THREE, Duration.standardSeconds(22)),
                        event(TestMessage.FOUR, Duration.standardMinutes(3)))
                .advanceWatermarkTo(baseTime.plus(Duration.standardMinutes(3)))
                .addElements(
                        event(TestMessage.ONE, Duration.standardMinutes(4)),
                        event(TestMessage.TWO, Duration.standardSeconds(270)))
                .advanceWatermarkToInfinity();

        PCollection<KV<String, Integer>> bookingCount = p.apply(createEvents)
                .apply(new BookingCountTimePipeline.CountBookingType(TEAM_WINDOW_DURATION, ALLOWED_LATENESS));

        PAssert.that(bookingCount)
                .inOnTimePane(new IntervalWindow(baseTime, TEAM_WINDOW_DURATION))
                .containsInAnyOrder(BOOKING_COUNT);

        p.run().waitUntilFinish();
    }

    @Test
    public void testBookingCountSpeculative() {
        TestStream<BookingMessage> createEvents = TestStream.create(AvroCoder.of(BookingMessage.class))
                .advanceWatermarkTo(baseTime)
                .addElements(
                        event(TestMessage.ONE, Duration.standardSeconds(3)),
                        event(TestMessage.TWO, Duration.standardMinutes(1)))
                .advanceProcessingTime(Duration.standardMinutes(10))
                .addElements(
                        event(TestMessage.THREE, Duration.standardMinutes(3)))
                .advanceProcessingTime(Duration.standardMinutes(12))
                .addElements(
                        event(TestMessage.FOUR, Duration.standardSeconds(22)))
                .advanceProcessingTime(Duration.standardMinutes(10))
                .addElements(
                        event(TestMessage.ONE, Duration.standardMinutes(4)),
                        event(TestMessage.TWO, Duration.standardMinutes(2)))
                .advanceWatermarkToInfinity();

        PCollection<KV<String, Integer>> bookingCount = p.apply(createEvents)
                .apply(new BookingCountTimePipeline.CountBookingType(TEAM_WINDOW_DURATION, ALLOWED_LATENESS));

        IntervalWindow window = new IntervalWindow(baseTime, TEAM_WINDOW_DURATION);

        // Speculative result
        PAssert.that(bookingCount)
                .inWindow(window)
                .containsInAnyOrder(BOOKING_COUNT_SPECULATIVE);

        // On time window result
        PAssert.that(bookingCount)
                .inOnTimePane(window)
                .containsInAnyOrder(BOOKING_COUNT);

        p.run().waitUntilFinish();
    }


}
