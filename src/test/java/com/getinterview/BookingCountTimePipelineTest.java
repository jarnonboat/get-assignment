package com.getinterview;

import avro.shaded.com.google.common.collect.Lists;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class BookingCountTimePipelineTest implements Serializable {
    static final String[] BOOKING_EVENT_ARRAY =
            new String[]{
                    "{\"order_number\": \"AP-1\",\"service_type\": \"GET_DELIVERY\",\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"JAKARTA\",\"payment_type\": \"GET_CASH\",\"status\": \"RIDING\",\"event_timestamp\": \"2018-03-29T13:05:00.000Z\"}",
                    "{\"order_number\": \"AP-1\",\"service_type\": \"GET_DELIVERY\",\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"JAKARTA\",\"payment_type\": \"GET_CASH\",\"status\": \"PENDING\",\"event_timestamp\": \"2018-03-29T13:00:00.000Z\"}",
                    "{\"order_number\": \"AP-3\",\"service_type\": \"GET_DELIVERY\",\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"JAKARTA\",\"payment_type\": \"GET_CASH\",\"status\": \"PENDING\",\"event_timestamp\": \"2018-03-29T13:00:00.000Z\"}",
                    "{\"order_number\": \"AP-4\",\"service_type\": \"GET_WIN\",\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"BANGKOK\",\"payment_type\": \"GET_CASH\",\"status\": \"COMPLETED\",\"event_timestamp\": \"2018-03-29T13:55:00.000Z\"}"
            };

    static final String[] BOOKING_EVENT_ARRAY2 =
            new String[]{
                    "{\"order_number\": \"AP-2\",\"service_type\": \"GET_WIN\",\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"BANGKOK\",\"payment_type\": \"GET_CREDIT\",\"status\": \"RIDING\",\"event_timestamp\": \"2018-03-29T12:25:00.000Z\"}",
                    "", // empty line
                    "{\"order_number\": \"AP-5\",\"service_type\": \"GET_WIN\"}", // valid format but key is missing
                    "{\"order_number\": incomplete json message" // incomplete json
            };

    static final List<String> BOOKING_EVENTS = Arrays.asList(BOOKING_EVENT_ARRAY);
    static final List<String> BOOKING_EVENTS2 = Arrays.asList(BOOKING_EVENT_ARRAY2);

    static final List<BookingMessage> BOOKING_MESSAGE_LIST =
            Lists.newArrayList(
                    new BookingMessage("AP-1", "GET_DELIVERY", "driver-123", "customer-123", "JAKARTA", "GET_CASH", "RIDING", "2018-03-29T13:05:00.000Z"),
                    new BookingMessage("AP-1", "GET_DELIVERY", "driver-123", "customer-123", "JAKARTA", "GET_CASH", "PENDING", "2018-03-29T13:00:00.000Z"),
                    new BookingMessage("AP-3", "GET_DELIVERY", "driver-123", "customer-123", "JAKARTA", "GET_CASH", "PENDING", "2018-03-29T13:00:00.000Z"),
                    new BookingMessage("AP-4", "GET_WIN", "driver-123", "customer-123", "BANGKOK", "GET_CASH", "COMPLETED", "2018-03-29T13:55:00.000Z")
            );

    static final List<BookingMessage> BOOKING_MESSAGE_LIST2 =
            Lists.newArrayList(
                    new BookingMessage("AP-2", "GET_WIN", "driver-123", "customer-123", "BANGKOK", "GET_CREDIT", "RIDING", "2018-03-29T12:25:00.000Z")
            );


    static final List<KV<String, Integer>> BOOKING_COUNT =
            Arrays.asList(
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,RIDING", 1),
                    KV.of("GET_DELIVERY,JAKARTA,GET_CASH,PENDING", 2),
                    KV.of("GET_WIN,BANGKOK,GET_CASH,COMPLETED", 1)
            );


    @Rule
    public TestPipeline p = TestPipeline.create();

    /**
     * Test that good input data is parsed to json properly.
     */
    @Test
    public void testValidJsonParser() {
        PCollection<String> input = p.apply(Create.of(BOOKING_EVENTS));
        PCollection<BookingMessage> output = input.apply(ParDo.of(new BookingCountTimePipeline.ParseBookingEventFn()));

        PAssert.that(output).containsInAnyOrder(BOOKING_MESSAGE_LIST);
        p.run().waitUntilFinish();
    }

    /**
     * Test that bad input data is dropped appropriately.
     */
    @Test
    public void testInValidJsonParse() {
        PCollection<String> input = p.apply(Create.of(BOOKING_EVENTS2));
        PCollection<BookingMessage> output = input.apply(ParDo.of(new BookingCountTimePipeline.ParseBookingEventFn()));

        PAssert.that(output).containsInAnyOrder(BOOKING_MESSAGE_LIST2);
        p.run().waitUntilFinish();
    }

    /**
     * Test extract and count the number of booking (get-report key)
     */
    @Test
    public void testBookingExtractAndCount() {
        PCollection<String> input = p.apply(Create.of(BOOKING_EVENTS));
        PCollection<KV<String, Integer>> output = input.apply(ParDo.of(new BookingCountTimePipeline.ParseBookingEventFn()))
                .apply("ExtractBookingCount", new BookingCountTimePipeline.ExtractAndCountBooking("get-report"));

        PAssert.that(output).containsInAnyOrder(BOOKING_COUNT);
        p.run().waitUntilFinish();
    }


}
