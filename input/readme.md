## Notes

* The input json file is not in the pretty format because I want to simulate the actual stream data from message broker.
* Normally, 1 json should represent 1 message. But for the sake of simplicity, I will use 1 json as 1 test case.
  * sampleNormalCase{1,2}.json = message with valid json format
  * sampleWithBroken = message with invalid and incomplete json