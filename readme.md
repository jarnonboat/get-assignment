# GET DE Interview assignment

For more detail answer: [Please check this document](https://docs.google.com/document/d/1UW7-h_GiAR0G4VeflJoguQjuKNZlL77o6WyCoOaa3Iw/edit?usp=sharing)


## Prerequisite
This project is built based on the followings
- JDK version 8
- Gradle
- Apache Beam's SDK

## Quick start
- make sure you have all prerequisite installed. The result should be somethings like this.
```
$ java -version
java version "1.8.0_211"
Java(TM) SE Runtime Environment (build 1.8.0_211-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.211-b12, mixed mode)
```
```$xslt
$ gradle -version
------------------------------------------------------------
Gradle 5.4
------------------------------------------------------------

Build time:   2019-04-16 02:44:16 UTC
Revision:     a4f3f91a30d4e36d82cc7592c4a0726df52aba0d

Kotlin:       1.3.21
Groovy:       2.5.4
Ant:          Apache Ant(TM) version 1.9.13 compiled on July 10 2018
JVM:          1.8.0_211 (Oracle Corporation 25.211-b12)
OS:           Mac OS X 10.14.4 x86_64
```
- Build the project

```
$ git clone https://gitlab.com/jarnonboat/get-assignment.git
$ cd get-assignment/
$ gradle clean
$ gradle build
```
- Test the project
```
# test everything
$ gradle test

# specific class
$ gradle test --tests com.getinterview.BookingCountTimePipelineTest
```
- Run the project with/without optional parameters
```
$ gradle run [--args='--key1=value1 --key2=value2 ...']

Available parameters
    --input             Path of the input file to read from
                        (default: input/sampleNormalCase1.json)
    --output            Path of the output file to write to
                        (default: output/output.csv)
    --writeKey          Boolean value to write key in output file (useful for debugging)
                        (default: false)
    --windowDuration    Numeric value of fixed window duration for analysis, in minutes
                        (default: 60)
    --allowedLateness   Numeric value of allowed data lateness, in minutes
                        (default: 120)
```
- The result will be in output folder
```
$ ls -la output/
total 32
-rw-r--r--  1 user  staff   127B Apr 25 23:51 output.csv-2018-03-29 12:00:00.000-2018-03-29 13:00:00.000-0-of-1
-rw-r--r--  1 user  staff   131B Apr 25 23:51 output.csv-2018-03-29 13:00:00.000-2018-03-29 14:00:00.000-0-of-1
-rw-r--r--  1 user  staff   135B Apr 25 23:51 output.csv-2018-03-29 14:00:00.000-2018-03-29 15:00:00.000-0-of-1
-rw-r--r--  1 user  staff    62B Apr 25 23:51 output.csv-2018-03-29 17:00:00.000-2018-03-29 18:00:00.000-0-of-1
```